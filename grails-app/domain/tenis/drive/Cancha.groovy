package tenis.drive


import valueObjects.Turno

import java.time.LocalDate

class Cancha {
    String numero;
    List<Reserva> reservas;

    static constraints = {
    }

    Cancha(String numero) {
        this.numero = numero;
        this.reservas = []
    }

    Reserva reservar(Cliente cliente, Turno turno, BigDecimal horasConLuz, ITipoReserva tipoReserva) {
        List<Reserva> reservasEnFecha = reservas.stream().filter({ reserva -> reserva.coincideEnDiaDeSemana(turno.obtenerDia()) && reserva.estaActiva() }).collect();

        Boolean reservado = reservasEnFecha.any({ reservaEnFecha -> reservaEnFecha.enHorario(turno) })

        if (reservado) {
            throw new Exception("Ya existe una reserva en esa fecha y horario")
        }

        Reserva reserva = new Reserva(cliente, turno, horasConLuz, tipoReserva)
        reservas << reserva;
        return reserva;
    }

    /**
     * cambia la ocurrencia ó todas las ocurrencias de la reserva por un nuevo turno
     * @param reserva
     * @param nuevoTurno
     * @return (nuevaReserva, penalizacion)
     */
    def modificarReserva(Reserva reserva, Turno nuevoTurno, BigDecimal horasConLuz) {
        reserva.cancelar();
        BigDecimal costoPenalizacion = reserva.calcularPenalizacion();
        try {
            Reserva nuevaReserva = reservar(reserva.getCliente(), nuevoTurno, horasConLuz, reserva.getTipoReserva())
            return [nuevaReserva, costoPenalizacion];
        } catch (Exception e) {
            reserva.reActivar()
            throw e
        }
    }

    /**
     * Retorna una nueva reserva en el nuevo turno, con una penalizacion de 50% si se quiere dar de baja en el mismo dia.
     * @param reserva
     * @param fechaACambiar
     * @param turnoNuevo
     * @param horasConLuz
     * @return (nuevaReserva, penalizacion)
     */
    def modificarReservaUnicaFecha(Reserva reserva, LocalDate fechaACambiar, Turno turnoNuevo, BigDecimal horasConLuz) {
        reserva.cancelarUnicaFecha(fechaACambiar);
        BigDecimal costoPenalizacion = reserva.calcularPenalizacion(fechaACambiar);
        try {
            Reserva nuevaReserva = reservar(reserva.getCliente(), turnoNuevo, horasConLuz, new ReservaEsporadica())
            return [nuevaReserva, costoPenalizacion];
        } catch (Exception e) {
            reserva.reActivarEnFecha(fechaACambiar)
            throw e
        }
    }

}
