package tenis.drive

import valueObjects.RangoHorario

import java.time.DayOfWeek
import java.time.LocalDate

class HorarioLaboral {
    DayOfWeek dia;
    RangoHorario rangoHorario;

    static embedded = ['rangoHorario']

    HorarioLaboral(DayOfWeek dia, RangoHorario rangoHorario) {
        this.dia = dia;
        this.rangoHorario = rangoHorario;
    }

    Boolean abiertoEn(LocalDate fecha, RangoHorario rangoHorario) {
        return this.dia == fecha.dayOfWeek && this.rangoHorario.contiene(rangoHorario)
    }
}
