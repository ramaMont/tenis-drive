package tenis.drive

import valueObjects.RangoHorario

import java.time.LocalDate

class Club {
    String nombre;
    String direccion;
    String email;
    String celular;
    String contraseña;
    List<HorarioLaboral> horariosLaborales;
    List<Cancha> canchas;

    //static hasMany = [horariosLaborales: HorarioLaboral]

    static constraints = {
        nombre blank: false, nullable: false;
        direccion blank: false, nullable: false;
        email blank: false, nullable: false;
        celular blank: false, nullable: false;
        contraseña blank: false, nullable: false;
    }

    Club(String nombre, String direccion, String email, String celular, String contraseña) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.email = email;
        this.celular = celular;
        this.contraseña = contraseña;
        this.horariosLaborales = [];
        this.canchas = []
    }

    void agregarHorarioLaboral(HorarioLaboral horarioLaboral) {
        !this.horariosLaborales.contains(horarioLaboral) ? this.horariosLaborales.add(horarioLaboral) : null;
    }

    void agregarCancha(Cancha cancha) {
        canchas << cancha
    }

    boolean abiertoEn(LocalDate fecha, RangoHorario rangoHorario) {
        return this.horariosLaborales.any { it.abiertoEn(fecha, rangoHorario) }
    }
}
