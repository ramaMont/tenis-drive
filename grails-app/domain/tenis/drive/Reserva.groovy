package tenis.drive

import groovy.transform.CompileStatic
import org.hibernate.mapping.OneToOne
import proveedores.ProveedorFecha
import valueObjects.Turno

import javax.persistence.FetchType
import java.time.DayOfWeek
import java.time.LocalDate

enum TipoReserva {
    FIJA({ ->
        new ReservaFija()
    }),
    ESPORADICA({ ->
        new ReservaEsporadica()
    });

    final Closure<ITipoReserva> fabricar;

    TipoReserva(Closure<ITipoReserva> fabrica) {
        this.fabricar = fabrica
    }

}

enum EstadoReserva {
    ACTIVA,
    CANCELADA
}

@CompileStatic
class Reserva {
    private static final BigDecimal COSTO_POR_HORA_LUZ = 200

    Cliente cliente;
    BigDecimal precio;
    Turno turno;
    BigDecimal horasConLuz;
    ITipoReserva tipoReserva;
    EstadoReserva estado;

    static constraints = {
    }

    static embedded = ['turno']

    Reserva(Cliente cliente, Turno turno, BigDecimal horasConLuz, ITipoReserva tipoReserva) {
        this.cliente = cliente;
        this.precio = cliente.obtenerCosto() * turno.rangoHorario.cantidadDeHoras() + horasConLuz * COSTO_POR_HORA_LUZ;
        this.horasConLuz = horasConLuz;
        this.tipoReserva = tipoReserva;
        this.estado = EstadoReserva.ACTIVA;
        this.turno = turno;
        this.tipoReserva.toString()
    }

    Boolean enHorario(Turno posibleTurno) {
        return tipoReserva.enHorario(turno, posibleTurno)
    }

    BigDecimal darDeBaja(LocalDate fecha) {
        return tipoReserva.darDeBaja(this, fecha)
    }

    void cancelar() {
        if (estado != EstadoReserva.ACTIVA) {
            throw new Exception("La reserva ya se encuentra cancelada")
        }
        this.estado = EstadoReserva.CANCELADA;
    }

    void cancelarUnicaFecha(LocalDate fechaACambiar) {
        tipoReserva.darDeBaja(this, fechaACambiar)
    }

    void reActivar() {
        this.estado = EstadoReserva.ACTIVA;
    }

    void reActivarEnFecha(LocalDate fechaAReactivar) {
        this.tipoReserva.reActivarEnFecha(this, fechaAReactivar);
    }

    Boolean estaActiva() {
        return this.estado == EstadoReserva.ACTIVA
    }

    Boolean coincideEnDiaDeSemana(DayOfWeek dia) {
        return turno.coincideEnDiaDeSemana(dia);
    }

    /**
     * Calcula la penalización de la reserva tomando como fecha de baja el dia del turno.
     * ESPORADICA   -> MISMA FECHA, penalizar
     * FIJA         -> MISMO DAY OF WEEK, penalizar
     *
     * @return BigDecimal
     */
    BigDecimal calcularPenalizacion() {
        return tipoReserva.penalizarCancelacionTotal(this) ? precio * 0.5 : 0;
    }

    /**
     * Calcula la penalización de la reserva recibiendo la fecha de baja por parámetro.
     * ESPORADICA   -> SI HOY == FECHA A CAMBIAR, penalizar
     * FIJA         -> SI HOY == FECHA A CAMBIAR, penalizar
     *
     * @return BigDecimal
     */
    BigDecimal calcularPenalizacion(LocalDate fechaDeBaja) {
        return ProveedorFecha.hoy() == fechaDeBaja ? precio * 0.5 : 0;
    }

    LocalDate getFecha() {
        return turno.getFecha();
    }

}
