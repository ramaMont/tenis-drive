package tenis.drive


import valueObjects.Turno

import java.time.LocalDate

class ITipoReserva {

    Boolean enHorario(Turno turnoReserva, Turno otroTurno){};

    void darDeBaja(Reserva reserva, LocalDate fecha){};

    Boolean penalizarCancelacionTotal(Reserva reserva){};

    void reActivarEnFecha(Reserva reserva, LocalDate fechaAReactivar){};

    @Override
    String toString(){};
}
