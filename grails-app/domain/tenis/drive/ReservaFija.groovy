package tenis.drive


import proveedores.ProveedorFecha
import valueObjects.Turno

import java.time.LocalDate

class ReservaFija extends ITipoReserva {
    List<LocalDate> fechasCanceladas;

    ReservaFija(){
        this.fechasCanceladas = [];
    }

    static constraints = {
    }

    Boolean enHorario(Turno turnoReserva, Turno otroTurno) {
        if (fechasCanceladas.contains(otroTurno.getFecha())) {
            return false
        }
        return turnoReserva.coincideEnDiaDeSemanaYHorario(otroTurno);
    }

    void darDeBaja(Reserva reserva, LocalDate fechaDeBaja) {
        if(!reserva.coincideEnDiaDeSemana(fechaDeBaja.getDayOfWeek())) {
            throw new Exception("La fecha de la reserva no coincide con la fecha de cancelación");
        }
        if(!fechasCanceladas.contains(fechaDeBaja)) {
            fechasCanceladas.add(fechaDeBaja)
        } else {
            throw new Exception("La reserva ya se encuentra cancelada en esa fecha");
        }
    }

    Boolean penalizarCancelacionTotal(Reserva reserva){
        return ProveedorFecha.hoy().getDayOfWeek() == reserva.getFecha().getDayOfWeek();
    }

    void reActivarEnFecha(Reserva reserva, LocalDate fechaAReactivar){
        if(!reserva.coincideEnDiaDeSemana(fechaAReactivar.getDayOfWeek())) {
            throw new Exception("El dia de la reserva no coincide con la fecha a reactivar");
        }
        if(fechasCanceladas.contains(fechaAReactivar)) {
            fechasCanceladas.remove(fechaAReactivar)
        }
    }

    @Override
    String toString() {
        return "Reserva Fija";
    }

}
