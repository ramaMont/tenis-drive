package tenis.drive

class TipoCliente {

    String nombre;
    BigDecimal costo;

   TipoCliente(String nombre, BigDecimal costo) {
      this.nombre = nombre;
      this.costo = costo;
   }

    static constraints = {
        nombre blank: false, nullable: false, unique: true;
        costo blank: false, nullable: false;
    }

    @Override
    String toString() {
        return "$nombre";
    }
}
