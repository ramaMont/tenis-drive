package tenis.drive


import proveedores.ProveedorFecha
import valueObjects.Turno

import java.time.LocalDate

class ReservaEsporadica extends ITipoReserva {

    static constraints = {
    }

    Boolean enHorario(Turno turnoReserva, Turno otroTurno) {
        return turnoReserva.coincideCon(otroTurno);
    }

    void darDeBaja(Reserva reserva, LocalDate fecha){
        reserva.cancelar();
    }

    Boolean penalizarCancelacionTotal(Reserva reserva){
        return reserva.getFecha() == ProveedorFecha.hoy();
    }

    void reActivarEnFecha(Reserva reserva, LocalDate fechaAReactivar){
        reserva.reActivar();
    }


    @Override
    String toString() {
        return "Reserva Esporadica";
    }

}
