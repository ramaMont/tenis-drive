package tenis.drive

class Cliente {
    String nombre;
    String apellido;
    String contraseña;
    String email;
    String celular;
    TipoCliente tipo;

    static constraints = {
    }

    Cliente(String nombre, String apellido, String contraseña, String email, String celular, TipoCliente tipo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.contraseña = contraseña;
        this.email = email;
        this.celular = celular;
        this.tipo = tipo;
    }

    BigDecimal obtenerCosto() {
        return tipo.costo
    }

    @Override
    String toString(){
        return "$nombre $apellido $email $celular $tipo";
    }
}
