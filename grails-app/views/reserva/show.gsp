<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'reserva.label', default: 'Reserva')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-reserva" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-reserva" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <fieldset class="form">
                <p>Tipo de reserva</p>
                <g:field name="tipoReserva" readonly="true" value="${reserva.tipoReserva}"/>
                <p>Cliente</p>
                <g:select name="cliente" readonly="true" from="${[reserva.cliente]}" />
                <p>Fecha</p>
                <g:field name="fecha" type="date"  readonly="true" value="${reserva.turno.fecha}"/>
                <p>Desde</p>
                <g:field name="horaInicio" type="time" readonly="true" value="${reserva.turno.rangoHorario.horaInicio}"/>
                <p>Hasta</p>
                <g:field name="horaFin" type="time" readonly="true" value="${reserva.turno.rangoHorario.horaFin}"/>
                <g:if test="${fechasCanceladas}">
                    <p>Fechas canceladas</p>
                    <g:select name="fechasCanceladas" readonly="true" from="${fechasCanceladas}"
                              optionValue="${{formatDate(format: 'dd/MM/yyyy', date: it)}}"/>
                </g:if>
                <p>Estado</p>
                <g:field name="estado" readonly="true" value="${reserva.estado}"/>
                <p>Horas con luz</p>
                <g:field name="horasConLuz" type="number"  readonly="true" value="${reserva.horasConLuz}"/>
                <p>Precio</p>
                <g:field name="precio" readonly="true" value="${reserva.precio}"/>
            </fieldset>



            %{--<table>
                <thead>
                <tr>
                    <th>Tipo</th>
                    <th>Cliente</th>
                    <th>Precio</th>
                    <th>Horas con luz</th>
                    <th>Turno</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>${tipoReserva.toString()}</td>
                        <td>${reserva.cliente}</td>
                        <td>${reserva.precio}</td>
                        <td>${reserva.horasConLuz}</td>
                        <td>${reserva.turno}</td>
                        <td>${reserva.estado}</td>
                        <td>TODO: MODIFICAR</td>
                    </tr>
                </tbody>
            </table>--}%

        </div>
    </body>
</html>
