<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'reserva.label', default: 'Reserva')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#create-reserva" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="create-reserva" class="content scaffold-create" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:if test="${flash.error}">
                <div class="errors" role="status">${flash.error}</div>
            </g:if>
            <g:hasErrors bean="${this.reserva}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.reserva}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form method="POST" action="reservar" controller="cancha" id="${params.id}">
                <fieldset class="form">
                    <p>Cliente</p>
                    <g:select name="cliente" optionKey="id" from="${clientes}" required="true" value="${flash.params?.cliente}"/>
                    <p>Fecha</p>
                    <g:field name="fecha" type="date"  required="true" value="${flash.params?.fecha}"/>
                    <p>Desde</p>
                    <g:field name="horaInicio" type="time" required="true" value="${flash.params?.horaInicio}"/>
                    <p>Hasta</p>
                    <g:field name="horaFin" type="time" required="true" value="${flash.params?.horaFin}"/>
                    <p>Horas con luz</p>
                    <g:field name="horasConLuz" type="number"  required="true" value="${flash.params?.horasConLuz}"/>
                    <p>Tipo de reserva</p>
                    <g:select name="tipoReserva" from="${tiposReserva}" required="true" value="${flash.params?.tipoReserva}"/>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="guardar" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
