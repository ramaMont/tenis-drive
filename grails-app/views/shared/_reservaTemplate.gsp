<div class="reserva" id="${reserva?.id}">
    <div>Tipo: ${reserva?.tipoReserva}</div>
    <div>Cliente: ${reserva?.cliente}</div>
    <div>Precio: ${reserva?.precio}</div>
    <div>Horas con luz: ${reserva?.horasConLuz}</div>
    <div>Turno: ${reserva?.turno}</div>
    <div>Estado: ${reserva?.estado}</div>
</div>