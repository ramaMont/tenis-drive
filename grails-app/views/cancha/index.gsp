<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cancha.label', default: 'Cancha')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-cancha" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-cancha" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:if test="${flash.warning}">
                <div class="message" role="status">${flash.warning}</div>
            </g:if>
            <table>
                <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <g:each var="cancha" in="${canchaList}">
                        <tr>
                            <td>
                                <a class="home" href="${createLink(uri: "/cancha/show/$cancha.id")}">
                                    ${cancha.numero}
                                </a>
                            </td>
                            <td>
                                <a class="home" href="${createLink(uri: "/cancha/$cancha.id/reserva/crear")}">
                                    Reservar
                                </a>
                                <br>
                                <a class="home" href="${createLink(uri: "/cancha/$cancha.id/reservas")}">
                                    Consultar reservas
                                </a>
                            </td>
                        </tr>
                    </g:each>
                </tbody>
            </table>

            <div class="pagination">
                <g:paginate total="${canchaCount ?: 0}" />
            </div>
        </div>
    </body>
</html>