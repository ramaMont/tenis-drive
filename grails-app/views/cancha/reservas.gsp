<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'cancha.label', default: 'Cancha')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            </ul>
        </div>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <table>
            <thead>
            <tr>
                <th>Tipo</th>
                <th>Cliente</th>
                <th>Turno</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="reserva" in="${reservas}">
                <tr>
                    <td>${reserva.tipoReserva}</td>
                    <td>${reserva.cliente}</td>
                    <td>${reserva.turno}</td>
                    <td>${reserva.estado}</td>
                    <td>
                        <a class="home" href="${createLink(uri: "/reserva/show/$reserva.id")}">
                            Detalles
                        </a>
                        <br/>
                        <a class="home" href="${createLink(uri: "/reserva/edit/$reserva.id")}">
                            Modificar
                        </a>
                        <br/>
                        <g:if test="${reserva.tipoReserva.instanceOf(tenis.drive.ReservaFija)}">
                            <a class="home" href="${createLink(uri: "/reserva/edit/$reserva.id/single")}">
                                Modificar unica fecha
                            </a>
                        </g:if>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </body>
</html>