package tenis.drive

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.LocalDate
import java.time.LocalTime

interface ICanchaService {

    Cancha get(Serializable id)

    List<Cancha> list(Map args)

    Long count()

    void delete(Serializable id)

    Cancha save(Cancha cancha)
}

@Service(Cancha)
abstract class CanchaService implements ICanchaService {

    ClienteService clienteService
    ClubService clubService

    Cancha findByReserva(Reserva reserva) {
        return Cancha.find("from Cancha as c where :reserva in elements(c.reservas)", [reserva: reserva]);
    }

    @Transactional
    def reservar(Serializable idCancha, Serializable idCliente, LocalDate fecha, LocalTime horaInicio, LocalTime horaFin, BigDecimal horasConLuz, String tipo) {
        Cliente cliente = clienteService.get(idCliente)
        ITipoReserva tipoReserva = TipoReserva.valueOf(tipo).fabricar()
        RangoHorario rangoHorario = new RangoHorario(horaInicio, horaFin)
        Turno turno = new Turno(fecha, rangoHorario)
        Cancha cancha = this.get(idCancha)
        Club club = clubService.findByCancha(cancha);
        if(!club.abiertoEn(fecha, rangoHorario)) {
            throw new Exception("El club no está abierto en ese horario");
        }
        return cancha.reservar(cliente, turno, new BigDecimal(horasConLuz), tipoReserva)
    }


}