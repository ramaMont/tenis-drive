package tenis.drive

import grails.gorm.services.Service

interface IClubService {

    Club get(Serializable id)

    List<Club> list(Map args)

    Long count()

    void delete(Serializable id)

    Club save(Club club)

}

@Service(Club)
abstract class ClubService implements IClubService {
    Club findByCancha(Cancha cancha) {
        return Club.find("from Club as c where :cancha in elements(c.canchas)", [cancha: cancha]);
    }

}