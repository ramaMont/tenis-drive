package tenis.drive

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.LocalDate
import java.time.LocalTime

interface IReservaService {

    Reserva get(Serializable id)

    List<Reserva> list(Map args);

    Long count();

    void delete(Serializable id);

    Reserva save(Reserva reserva);

}

@Service(Reserva)
abstract class ReservaService implements IReservaService {

    CanchaService canchaService;
    ClubService clubService;

    @Transactional
    def modificar(Serializable reservaId, Serializable canchaId, LocalDate fecha, LocalTime horaInicio, LocalTime horaFin, BigDecimal horasConLuz) {
        Reserva reserva = this.get(reservaId);
        Cancha cancha = canchaId ? Cancha.get(canchaId) : canchaService.findByReserva(reserva);
        Club club = clubService.findByCancha(cancha);
        RangoHorario rangoHorario = new RangoHorario(horaInicio, horaFin);
        if(!club.abiertoEn(fecha, rangoHorario)) {
            throw new Exception("El club no está abierto en ese horario");
        }
        return cancha.modificarReserva(reserva, new Turno(fecha, rangoHorario), horasConLuz)
    }

    @Transactional
    def modificarUnicaVez(Serializable reservaId, Serializable canchaId, LocalDate fechaACambiar, LocalDate fechaNueva, LocalTime horaInicio, LocalTime horaFin, BigDecimal horasConLuz) {
        Reserva reserva = this.get(reservaId);
        Cancha cancha = canchaId ? Cancha.get(canchaId) : canchaService.findByReserva(reserva);
        Club club = clubService.findByCancha(cancha);
        RangoHorario rangoHorario = new RangoHorario(horaInicio, horaFin);
        if(!club.abiertoEn(fechaNueva, rangoHorario)) {
            throw new Exception("El club no está abierto en ese horario");
        }
        return cancha.modificarReservaUnicaFecha(reserva, fechaACambiar, new Turno(fechaNueva, rangoHorario), horasConLuz)
    }
}







