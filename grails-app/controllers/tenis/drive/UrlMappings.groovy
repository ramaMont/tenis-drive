package tenis.drive

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/cancha/$id/reservas"(controller: 'cancha', action: 'reservas')
        "/reserva/edit/$id/single"(controller: 'reserva', action: 'editSingle')
        "/cancha/$id/reserva/crear"(controller: 'reserva', action: 'create')

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
