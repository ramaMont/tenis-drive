package tenis.drive

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class TipoClienteController {

    TipoClienteService tipoClienteService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond tipoClienteService.list(params), model:[tipoClienteCount: tipoClienteService.count()]
    }

    def show(Long id) {
        respond tipoClienteService.get(id)
    }

    def create() {
        respond new TipoCliente(params)
    }

    def save(TipoCliente tipoCliente) {
        if (tipoCliente == null) {
            notFound()
            return
        }

        try {
            tipoClienteService.save(tipoCliente)
        } catch (ValidationException e) {
            respond tipoCliente.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tipoCliente.label', default: 'TipoCliente'), tipoCliente.id])
                redirect tipoCliente
            }
            '*' { respond tipoCliente, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond tipoClienteService.get(id)
    }

    def update(TipoCliente tipoCliente) {
        if (tipoCliente == null) {
            notFound()
            return
        }

        try {
            tipoClienteService.save(tipoCliente)
        } catch (ValidationException e) {
            respond tipoCliente.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tipoCliente.label', default: 'TipoCliente'), tipoCliente.id])
                redirect tipoCliente
            }
            '*'{ respond tipoCliente, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        tipoClienteService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'tipoCliente.label', default: 'TipoCliente'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipoCliente.label', default: 'TipoCliente'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
