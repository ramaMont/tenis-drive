package tenis.drive

import grails.validation.ValidationException
import valueObjects.RangoHorario

import java.time.DayOfWeek
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle

import static java.util.Locale.forLanguageTag
import static org.springframework.http.HttpStatus.*

class HorarioLaboralController {

    HorarioLaboralService horarioLaboralService
    ClubService clubService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond horarioLaboralService.list(params), model:[horarioLaboralCount: horarioLaboralService.count()]
    }

    def show(Long id) {
        HorarioLaboral horarioLaboral = horarioLaboralService.get(id);
        String diaSemana = horarioLaboral.getDia().getDisplayName(TextStyle.FULL, new Locale("ES"));
        diaSemana = diaSemana.substring(0, 1).toUpperCase() + diaSemana.substring(1);
        [dia: diaSemana, rangoHorario: horarioLaboral.getRangoHorario()]
    }

    def create() {
        //respond new HorarioLaboral(params)
        [horarioLaboral: new HorarioLaboral(params), club_id: params["club.id"]]
    }

    def save(HorarioLaboral horarioLaboral) {
        if (horarioLaboral == null) {
            notFound()
            return
        }

        try {
            horarioLaboralService.save(horarioLaboral)
        } catch (ValidationException e) {
            respond horarioLaboral.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'horarioLaboral.label', default: 'HorarioLaboral'), horarioLaboral.id])
                redirect horarioLaboral
            }
            '*' { respond horarioLaboral, [status: CREATED] }
        }
    }

    def guardar() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE", forLanguageTag("es"));
        DayOfWeek dia = DayOfWeek.from(formatter.parse(params.dia.toLowerCase()));
        RangoHorario rangoHorario = new RangoHorario(LocalTime.parse(params.horaInicio), LocalTime.parse(params.horaFin));

        HorarioLaboral horarioLaboral = new HorarioLaboral(dia, rangoHorario)

        try {
            horarioLaboralService.save(horarioLaboral)
        } catch (ValidationException e) {
            respond horarioLaboral.errors, view:'create'
            return
        }

        Club club = clubService.get(params.club_id)
        club.agregarHorarioLaboral(horarioLaboral)
        clubService.save(club)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'horarioLaboral.label', default: 'HorarioLaboral'), horarioLaboral.id])
                redirect horarioLaboral
            }
            '*' { respond horarioLaboral, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond horarioLaboralService.get(id)
    }

    def update(HorarioLaboral horarioLaboral) {
        if (horarioLaboral == null) {
            notFound()
            return
        }

        try {
            horarioLaboralService.save(horarioLaboral)
        } catch (ValidationException e) {
            respond horarioLaboral.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'horarioLaboral.label', default: 'HorarioLaboral'), horarioLaboral.id])
                redirect horarioLaboral
            }
            '*'{ respond horarioLaboral, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        horarioLaboralService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'horarioLaboral.label', default: 'HorarioLaboral'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'horarioLaboral.label', default: 'HorarioLaboral'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
