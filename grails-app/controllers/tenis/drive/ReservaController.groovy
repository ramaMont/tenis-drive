package tenis.drive

import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import org.hibernate.Hibernate
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.LocalDate
import java.time.LocalTime

import static org.springframework.http.HttpStatus.*


class ReservaController {

    CanchaService canchaService
    ClubService clubService
    ReservaService reservaService
    ClienteService clienteService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond reservaService.list(params), model: [reservaCount: reservaService.count()]
    }

    def show(Long id) {
        def data = [:]
        Reserva reserva = reservaService.get(id)
        ITipoReserva tipoReserva = Hibernate.unproxy(reserva.getTipoReserva())
        if (tipoReserva.instanceOf(ReservaFija)) {
            ReservaFija reservaFija = (ReservaFija)tipoReserva
            data["fechasCanceladas"] = reservaFija.getFechasCanceladas()
        }
        data["reserva"] = reserva
        return data
    }

    def create(Long id) {
        [
                reserva: new Reserva(params),
                clientes: clienteService.list(),
                tiposReserva: TipoReserva.values()
        ]
    }

    def edit(Long id) {
        Reserva reserva = reservaService.get(id)
        Cancha cancha = canchaService.findByReserva(reserva)
        Club club = clubService.findByCancha(cancha)
        [reserva: reserva, listaCanchas: club.canchas, idCancha: cancha.id]
    }

    def editSingle(Long id) {
        Reserva reserva = reservaService.get(id)
        Cancha cancha = canchaService.findByReserva(reserva)
        Club club = clubService.findByCancha(cancha)
        [reserva: reserva, listaCanchas: club.canchas, idCancha: cancha.id]
    }

    def modificarUnicaVez(Long id) {

        def reserva, penalizacion;

        try {
            (reserva, penalizacion) = reservaService.modificarUnicaVez(id, params.idCancha, LocalDate.parse(params.fechaACambiar), LocalDate.parse(params.fechaNueva), LocalTime.parse(params.horaInicio), LocalTime.parse(params.horaFin), new BigDecimal(params.horasConLuz))
        } catch (Exception e) {
            flash.error = e.message
            redirect(action: 'editSingle', id: id)
            return
        }

        redirect(controller: 'cancha', action: 'reservas', id: params.idCancha)
    }

    def modificar(Long id) {

        def reserva, penalizacion;

        try {
            (reserva, penalizacion) = reservaService.modificar(id, params.idCancha, LocalDate.parse(params.fecha), LocalTime.parse(params.horaInicio), LocalTime.parse(params.horaFin), new BigDecimal(params.horasConLuz))
        } catch (Exception e) {
            flash.error = e.message
            redirect(action: 'edit', id: id)
            return
        }

        flash.message = "Penalizacion: ${penalizacion}"
        redirect(controller: 'cancha', action: 'reservas', id: params.idCancha)

    }


    def update(Reserva reserva) {
        if (reserva == null) {
            notFound()
            return
        }

        try {
            reservaService.save(reserva)
        } catch (ValidationException e) {
            respond reserva.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'reserva.label', default: 'Reserva'), reserva.id])
                redirect reserva
            }
            '*' { respond reserva, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        reservaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'reserva.label', default: 'Reserva'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'reserva.label', default: 'Reserva'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
