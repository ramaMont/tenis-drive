package tenis.drive

import grails.validation.ValidationException
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.LocalDate
import java.time.LocalTime

import static org.springframework.http.HttpStatus.*

class CanchaController {

    CanchaService canchaService
    ClubService clubService
    ClienteService clienteService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond canchaService.list(params), model:[canchaCount: canchaService.count()]
    }

    def reservas(Long id) {
        Cancha cancha = canchaService.get(id)
        [cancha: cancha, reservas: cancha.reservas]
    }

    def show(Long id) {
        respond canchaService.get(id)
    }

    def create() {
        [cancha: new Cancha(params), club_id: params["club.id"]]
    }

    def save(Cancha cancha) {
        if (cancha == null) {
            notFound()
            return
        }

        try {
            canchaService.save(cancha)
        } catch (ValidationException e) {
            respond cancha.errors, view:'create'
            return
        }

        Club club = clubService.get(params.club_id)
        club.agregarCancha(cancha)
        clubService.save(club)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'cancha.label', default: 'Cancha'), cancha.id])
                redirect cancha
            }
            '*' { respond cancha, [status: CREATED] }
        }
    }


    def edit(Long id) {
        respond canchaService.get(id)
    }

    def update(Cancha cancha) {
        if (cancha == null) {
            notFound()
            return
        }

        try {
            canchaService.save(cancha)
        } catch (ValidationException e) {
            respond cancha.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'cancha.label', default: 'Cancha'), cancha.id])
                redirect cancha
            }
            '*'{ respond cancha, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        canchaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'cancha.label', default: 'Cancha'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'cancha.label', default: 'Cancha'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def reservar() {

        Reserva reserva = null;

        try {
            LocalDate fecha = LocalDate.parse(params.fecha)
            LocalTime horaInicio = LocalTime.parse(params.horaInicio)
            LocalTime horaFin = LocalTime.parse(params.horaFin)

            reserva = canchaService.reservar(params.id, params.cliente, fecha, horaInicio, horaFin, new BigDecimal(params.horasConLuz), params.tipoReserva)
        } catch (Exception e) {
            flash.error = e.message
            flash.params = params
            redirect(controller: "reserva", action: "create", id:params.id)
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'reserva.label', default: 'Reserva'), reserva.id])
                redirect action: "reservas", method: "GET", id: params.id
            }
            '*' { respond reserva, [status: CREATED] }
        }
    }


}
