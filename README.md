# Tenis Drive

## Requisitos
Grails Version: 4.1.0

JVM Version: 11.0.15

## Comandos
Para levantar el proyecto: `grails run-app` (en localhost:8080)

Para agregar una clase del dominio: `grails create-domain-class <Clase>`

Para crearle controller a una clase: `grails create-scaffold-controller <Clase>`

Correr tests: `grails test-app`

Acceder a la DB: `http://localhost:8080/h2-console/` -> En url de db: `jdbc:h2:mem:devDb`
