package tenis.drive

import grails.test.hibernate.HibernateSpec

class TipoClienteSpec extends HibernateSpec {

    def setup() {
    }

    def cleanup() {
    }

    void "creación exitosa"() {
        def tipo = new TipoCliente("Particular", 1200);
        expect:
        tipo.getCosto() == 1200;
    }

    void "creación fallida por nombre repetido"() {
        given:
        def tipo = new TipoCliente("Particular", 1200);
        and:
        tipo.save();
        when:
        def tipoDuplicado = new TipoCliente("Particular", 1600);
        then:
        !tipoDuplicado.validate(['nombre']);
        and:
        !tipoDuplicado.save();
    }
}
