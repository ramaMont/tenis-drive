package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class ReservaEsporadicaSpec extends Specification implements DomainUnitTest<ReservaEsporadica> {

    def setup() {
    }

    def cleanup() {
    }

    void "en horario es true con la misma fecha y rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        expect:
        new ReservaEsporadica().enHorario(turno, turno)
    }

    void "en horario es false con la misma fecha y distinto rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        RangoHorario rangoDistinto = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("10:00"));
        Turno turno = new Turno(fecha, rango);
        Turno otroTurno = new Turno(fecha, rangoDistinto);
        expect:
        !new ReservaEsporadica().enHorario(turno, otroTurno)
    }


    void "en horario es false con distinta fecha y mismo rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        LocalDate fechaDistinta = LocalDate.now().with(DayOfWeek.FRIDAY);
        Turno turno = new Turno(fecha, rango);
        Turno otroTurno = new Turno(fechaDistinta, rango);
        expect:
        !new ReservaEsporadica().enHorario(turno, otroTurno)
    }
}
