package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class ReservaFijaSpec extends Specification implements DomainUnitTest<ReservaFija> {

    def setup() {
    }

    def cleanup() {
    }

    void "en horario es true con la misma fecha y rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        expect:
        new ReservaFija().enHorario(turno, turno)
    }

    void "en horario es false con la misma fecha y distinto rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        RangoHorario rangoDistinto = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("10:00"))
        expect:
        !new ReservaFija().enHorario(new Turno(fecha, rango), new Turno(fecha, rangoDistinto))
    }

    void "en horario es false con distinta fecha y mismo rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        LocalDate fechaDistinta = LocalDate.now().with(DayOfWeek.FRIDAY);
        expect:
        !new ReservaFija().enHorario(new Turno(fecha, rango), new Turno(fechaDistinta, rango))
    }

    void "en horario es true con distinta fecha una semana despues y mismo rango horario"() {
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        LocalDate fechaDistinta = fecha.plusWeeks(1);
        expect:
        new ReservaFija().enHorario(new Turno(fecha, rango), new Turno(fechaDistinta, rango));
    }

    void "en horario es false con distinta fecha una semana despues dada de baja y mismo rango horario"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        ReservaFija reservaFija = new ReservaFija()
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Reserva reserva = new Reserva(cliente, new Turno(fecha, rango), 0, reservaFija);
        LocalDate fechaDistinta = fecha.plusWeeks(1);
        reservaFija.darDeBaja(reserva, fechaDistinta);
        expect:
        !reservaFija.enHorario(new Turno(fecha, rango), new Turno(fechaDistinta, rango));
        reservaFija.enHorario(new Turno(fecha, rango), new Turno(fecha.plusWeeks(2), rango));
    }
}
