package tenis.drive

import grails.testing.gorm.DomainUnitTest
import proveedores.ProveedorFecha
import spock.lang.Specification
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class CanchaSpec extends Specification implements DomainUnitTest<Cancha> {

    def setup() {}

    def cleanup() {}

    void "cancha se crea correctamente"() {
        def cancha = new Cancha("1")
    }

    void "admite reserva para fecha y horario disponible"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        expect:
        cancha.getReservas().size() == 1
    }

    void "lanza error al coincidir fecha y horario de otra reserva esporádica"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        when:
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        then:
        def e = thrown(Exception)
        e.message == "Ya existe una reserva en esa fecha y horario"
    }

    void "lanza error al coincidir fecha una semana despues mismo horario de otra reserva fija"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        when:
        cancha.reservar(cliente, new Turno(fecha.plusWeeks(1), rango), 0, new ReservaEsporadica())
        then:
        def e = thrown(Exception)
        e.message == "Ya existe una reserva en esa fecha y horario"
    }

    void "se puede reservar en una fecha y horario de reserva fija dada de baja"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        reserva.darDeBaja(fecha)
        when:
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        then:
        noExceptionThrown()
        cancha.getReservas().size() == 2
    }

    void "se puede reservar en una fecha y horario de reserva esporadica dada de baja"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        reserva.darDeBaja(fecha)
        when:
        cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        then:
        noExceptionThrown()
        cancha.getReservas().size() == 2
    }

    void "se modifica una reserva fija en misma cancha sin penalización"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        LocalDate fechaActual = LocalDate.of(2020, 1, 1);
        ProveedorFecha proveedor = ProveedorFecha.establecerFechaFalsa(fechaActual)
        expect:
        def (_, penalizacion) = cancha.modificarReserva(reserva, new Turno(fecha.plusWeeks(1), rango), 0)
        penalizacion == 0
        cancha.getReservas().size() == 2
        cancha.getReservas().get(0).estaActiva() == false
        cancha.getReservas().get(1).estaActiva() == true
    }

    void "se modifica una reserva fija mismo turno"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        LocalDate fechaActual = LocalDate.of(2020, 1, 1);
        ProveedorFecha proveedor = ProveedorFecha.establecerFechaFalsa(fechaActual)

        expect:
        def (reservaNueva, penalizacion) = cancha.modificarReserva(reserva, new Turno(fecha, rango), 1)
        penalizacion == 0
        cancha.getReservas().size() == 2
        reserva.estaActiva() == false
        reservaNueva.estaActiva() == true
    }

    void "se modifica una reserva fija en misma cancha con penalización"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaFija())
        ProveedorFecha.establecerFechaFalsa(fecha)
        expect:
        def (_, penalizacion) = cancha.modificarReserva(reserva, new Turno(fecha.plusWeeks(1), rango), 0)
        penalizacion == 600
    }

    void "se modifica una reserva esporadica en misma cancha con penalización"() {
        TipoCliente tipoCliente = new TipoCliente("Particular", 1400);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fecha, rango), 0, new ReservaEsporadica())
        ProveedorFecha.establecerFechaFalsa(fecha)
        expect:
        def (_, penalizacion) = cancha.modificarReserva(reserva, new Turno(fecha.plusWeeks(1), rango), 0)
        penalizacion == 700
    }

    void "se modifica una reserva fija en misma cancha y se agregan reservas esporadicas en 2 fechas de la fija"() {
        given: 'una resera fija en la fechaReserva'
        TipoCliente tipoCliente = new TipoCliente("Particular", 1400);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fechaReserva = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fechaReserva, rango), 0, new ReservaFija())
        LocalDate fechaActual = LocalDate.of(2020, 1, 1);
        ProveedorFecha.establecerFechaFalsa(fechaActual)
        RangoHorario nuevoRango = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("10:00"));
        def (_, penalizacion) = cancha.modificarReserva(reserva, new Turno(fechaReserva, nuevoRango), 0)
        cancha.reservar(cliente, new Turno(fechaReserva, rango), 0, new ReservaEsporadica())
        when:
        cancha.reservar(cliente,
                new Turno(fechaReserva.plusWeeks(1), new RangoHorario(LocalTime.parse("08:00"),
                        LocalTime.parse("09:30"))), 0, new ReservaEsporadica());
        then:
        def e = thrown(Exception)
        e.message == "Ya existe una reserva en esa fecha y horario"
        penalizacion == 0
    }

    void "se modifica una reserva fija una unica fecha"() {
        given: 'una reserva fija en la fechaReserva'
        TipoCliente tipoCliente = new TipoCliente("Particular", 1400);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fechaReserva = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fechaReserva, rango), 0, new ReservaFija())
        LocalDate fechaActual = LocalDate.of(2020, 1, 1);
        ProveedorFecha.establecerFechaFalsa(fechaActual)
        RangoHorario nuevoRango = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("10:00"));
        Turno nuevoTurno = new Turno(fechaReserva.plusWeeks(2).plusDays(1), nuevoRango)
        when:
        def (nuevaReserva, penalizacion) = cancha.modificarReservaUnicaFecha(reserva, fechaReserva.plusWeeks(2), nuevoTurno, 0)
        cancha.reservar(cliente, new Turno(fechaReserva.plusWeeks(2), rango), 0, new ReservaEsporadica())
        then:
        penalizacion == 0
        nuevaReserva.getTurno().getFecha() == fechaReserva.plusWeeks(2).plusDays(1)
    }

    void "intentar reservar en fecha fija no modificada lanza excepcion por estar ocupada"() {
        given: 'una reserva fija en la fechaReserva'
        TipoCliente tipoCliente = new TipoCliente("Particular", 1400);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fechaReserva = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Cancha cancha = new Cancha("1")
        Reserva reserva = cancha.reservar(cliente, new Turno(fechaReserva, rango), 0, new ReservaFija())
        LocalDate fechaActual = LocalDate.of(2020, 1, 1);
        ProveedorFecha.establecerFechaFalsa(fechaActual)
        RangoHorario nuevoRango = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("10:00"));
        Turno nuevoTurno = new Turno(fechaReserva.plusWeeks(2).plusDays(1), nuevoRango)
        def (nuevaReserva, penalizacion) = cancha.modificarReservaUnicaFecha(reserva, fechaReserva.plusWeeks(2), nuevoTurno, 0)
        when:
        cancha.reservar(cliente, new Turno(fechaReserva, rango), 0, new ReservaEsporadica())
        then:
        def e = thrown(Exception)
        e.message == "Ya existe una reserva en esa fecha y horario"
        penalizacion == 0
    }

}
