package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import valueObjects.RangoHorario

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class HorarioLaboralSpec extends Specification implements DomainUnitTest<HorarioLaboral> {

    def setup() {
    }

    def cleanup() {
    }

    void "abiertoEn es correcto"() {
        RangoHorario rango = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def horarioLaboral1 = new HorarioLaboral(DayOfWeek.MONDAY, rango);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY)
        expect:"El horario laboral 1 está abierto en el horario laboral 2"
            horarioLaboral1.abiertoEn(fecha, rango) == true;
    }

    void "abiertoEn es falso"() {
        RangoHorario rangoAbierto = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        RangoHorario rangoTest = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("19:00"));
        def horarioLaboral1 = new HorarioLaboral(DayOfWeek.MONDAY, rangoAbierto);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY)
        expect:"El horario laboral 1 está abierto en el horario laboral 2"
        horarioLaboral1.abiertoEn(fecha, rangoTest) == false;
    }
}
