package tenis.drive

import spock.lang.Specification
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.LocalDate
import java.time.LocalTime

class TurnoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "turno no coincide con otro turno en fecha distinta"() {
        LocalDate fecha = LocalDate.now();
        RangoHorario rangoHorario = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        Turno turno1 = new Turno(fecha, rangoHorario);
        Turno turno2 = new Turno(fecha.plusDays(1), rangoHorario);
        expect:"El turno 1 no coincide con el turno 2"
            turno1.coincideCon(turno2) == false;
    }

    void "turno coincide con otro turno en misma fecha y rango horario"() {
        LocalDate fecha = LocalDate.now();
        RangoHorario rangoHorario = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        Turno turno1 = new Turno(fecha, rangoHorario);
        Turno turno2 = new Turno(fecha, rangoHorario);
        expect:"El turno 1 coincide con el turno 2"
        turno1.coincideCon(turno2) == true;
    }


}
