package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import valueObjects.RangoHorario
import valueObjects.Turno

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class ReservaSpec extends Specification implements DomainUnitTest<Reserva> {

    def setup() {
    }

    def cleanup() {
    }

    void "es creada correctamente"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 0, new ReservaEsporadica());
        then:
        noExceptionThrown()
    }

    void "cálculo de precio reserva cancha a socio es correcto"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Socio", 1000);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 0, new ReservaEsporadica());
        then:
        reserva.getPrecio() == 1000
    }

    void "cálculo de precio reserva cancha a particular es correcto"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 0, new ReservaEsporadica());
        then:
        reserva.getPrecio() == 1200
    }

    void "cálculo de precio reserva cancha a profesor es correcto"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Profesor", 900);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 0, new ReservaEsporadica());
        then:
        reserva.getPrecio() == 900
    }

    void "cálculo de precio reserva cancha a socio con extra por iluminación es correcto"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Socio", 1000);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 1, new ReservaEsporadica());
        then:
        reserva.getPrecio() == 1200
    }

    void "cálculo de precio reserva cancha a particular con extra por iluminación es correcto"() {
        given:
        TipoCliente tipoCliente = new TipoCliente("Particular", 1200);
        Cliente cliente = new Cliente("Juan", "Perez", "123", "asd@mgnails.cds", "123456789", tipoCliente);
        LocalDate fecha = LocalDate.now().with(DayOfWeek.MONDAY);
        RangoHorario rango = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("10:00"));
        Turno turno = new Turno(fecha, rango);
        when:
        Reserva reserva = new Reserva(cliente, turno, 1, new ReservaEsporadica());
        then:
        reserva.getPrecio() == 2600
    }


}
