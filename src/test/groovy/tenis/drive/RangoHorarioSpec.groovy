package tenis.drive

import spock.lang.Specification
import valueObjects.RangoHorario

import java.time.LocalTime

class RangoHorarioSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "ocupa totalmente es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("13:00"), LocalTime.parse("15:00"));
        expect:"El rango horario 2 no está libre ya que está ocupado en el rango horario 1"
            rangoHorario2.coincideCon(rangoHorario1) == true;
    }

    void "ocupa parcialmente rango derecho es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("13:00"), LocalTime.parse("20:00"));
        expect:"El rango horario 2 no está libre ya que está ocupado en el rango horario 1"
        rangoHorario2.coincideCon(rangoHorario1) == true;
    }

    void "ocupa parcialmente rango izquierdo es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("16:00"));
        expect:"El rango horario 2 no está libre ya que está ocupado en el rango horario 1"
        rangoHorario2.coincideCon(rangoHorario1) == true;
    }

    void "no ocupa rango izquierdo es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("08:00"), LocalTime.parse("09:00"));
        expect:"El rango horario 2 no ocupa el rango horario 1"
        rangoHorario2.coincideCon(rangoHorario1) == false;
    }


    void "no ocupa rango derecho es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("19:00"), LocalTime.parse("22:00"));
        expect:"El rango horario 2 no ocupa el rango horario 1"
        rangoHorario2.coincideCon(rangoHorario1) == false;
    }

    void "ocupa mas que totalmente es correcto"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("09:00"), LocalTime.parse("20:00"));
        expect:"El rango horario 2 no está libre ya que está ocupado en el rango horario 1"
        rangoHorario2.coincideCon(rangoHorario1) == true;
    }


    void "ocupa exactamente el mismo rango es correcto"() {
        RangoHorario rango = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        expect:"El rango horario no está libre ya que está ocupado en el rango horario"
        rango.coincideCon(rango) == true;
    }

    void "contiene otro rango horario"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("11:00"), LocalTime.parse("18:00"));
        expect:"El rango horario 1 contiene al rango horario 2"
        rangoHorario1.contiene(rangoHorario2) == true;
    }

    void "no contiene otro rango horario"() {
        def rangoHorario1 = new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("18:00"));
        def rangoHorario2 = new RangoHorario(LocalTime.parse("11:00"), LocalTime.parse("18:01"));
        expect:"El rango horario 1 no contiene al rango horario 2"
        rangoHorario1.contiene(rangoHorario2) == false;
    }


}
