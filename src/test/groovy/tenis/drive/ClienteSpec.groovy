package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class ClienteSpec extends Specification implements DomainUnitTest<Cliente> {

    def setup() {
    }

    def cleanup() {
    }

    void "cliente socio se crea correctamente"() {
        def cliente = new Cliente("Juan", "Paredes", "asd123", "juanp@p.com", "1150113280", new TipoCliente("Socio", 1000))
        expect:
            cliente.obtenerCosto() == 1000
    }
}
