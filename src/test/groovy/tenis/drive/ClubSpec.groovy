package tenis.drive

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import valueObjects.RangoHorario

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalTime

class ClubSpec extends Specification implements DomainUnitTest<Club> {

    def club

    def setup() {
        club = new Club("Club de Tenis", "Calle falsa 123", "hola@gmail.com", "123123123", "abc123");
    }

    def cleanup() {
    }

    void "agregar horario es correcto"(){
        def horarioLaboral = new HorarioLaboral(DayOfWeek.MONDAY, new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("12:00")));
        club.agregarHorarioLaboral(horarioLaboral);
        expect:"El horario laboral fue agregado correctamente"
            club.horariosLaborales.size() == 1;
        and:"El horario laboral que se agregó es el correcto"
            club.horariosLaborales.get(0) == horarioLaboral;
        and:"Se encuentra abierto el dia de la semana"
            club.abiertoEn(LocalDate.now().with(DayOfWeek.MONDAY), new RangoHorario(LocalTime.parse("10:00"), LocalTime.parse("11:00")))
    }

    void "cancha se agrega correctamente"(){
        club.agregarCancha(new Cancha("1"))
        expect: 
            club.getCanchas().size == 1
    }
}
