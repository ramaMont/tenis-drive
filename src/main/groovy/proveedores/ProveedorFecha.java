package proveedores;

import groovy.lang.Singleton;

import java.time.LocalDate;

@Singleton
public class ProveedorFecha {
  static Boolean usaFechaFalsa = false;
  static LocalDate fechaFalsa = null;

  static void establecerFechaFalsa(LocalDate fecha) {
    usaFechaFalsa = true;
    fechaFalsa = fecha;
  }

  public static LocalDate hoy() {
    return usaFechaFalsa ? fechaFalsa : LocalDate.now();
  }

  static void resetear() {
    usaFechaFalsa = false;
  }

}
