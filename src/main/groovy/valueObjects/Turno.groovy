package valueObjects

import java.time.DayOfWeek
import java.time.LocalDate

class Turno {
    LocalDate fecha;
    RangoHorario rangoHorario;

    static embedded = ['rangoHorario']

    Turno(){
    }

    Turno(LocalDate fecha, RangoHorario rangoHorario) {
        this.fecha = fecha;
        this.rangoHorario = rangoHorario;
    }

    Boolean coincideCon(Turno otroTurno) {
        return otroTurno.coincideCon(fecha, rangoHorario)
    }

    Boolean coincideCon(LocalDate fecha, RangoHorario rangoHorario) {
        return this.fecha == fecha && this.rangoHorario.coincideCon(rangoHorario);
    }
    Boolean coincideEnDiaDeSemanaYHorario(Turno otroTurno) {
        return otroTurno.coincideEnDiaDeSemanaYHorario(fecha.getDayOfWeek(), rangoHorario)
    }

    Boolean coincideEnDiaDeSemanaYHorario(DayOfWeek diaDeSemana, RangoHorario rangoHorario) {
        return this.coincideEnDiaDeSemana(diaDeSemana) && this.rangoHorario.coincideCon(rangoHorario);
    }

    Boolean coincideEnDiaDeSemana(DayOfWeek dia){
        return fecha.getDayOfWeek() == dia
    }

    DayOfWeek obtenerDia(){
        return fecha.getDayOfWeek()
    }

    @Override
    public String toString() {
        return "fecha=" + fecha + ", rangoHorario=" + rangoHorario;
    }
}