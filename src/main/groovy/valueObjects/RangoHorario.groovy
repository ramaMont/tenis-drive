package valueObjects

import groovy.time.Duration

import java.time.LocalTime

import static java.time.temporal.ChronoUnit.MINUTES

class RangoHorario {
    LocalTime horaInicio;
    LocalTime horaFin;

    RangoHorario(){
    }

    RangoHorario(LocalTime horaInicio, LocalTime horaFin) {
        if(horaFin < horaInicio) {
            throw new Exception("La duracion de fin es menor que la de inicio");
        }
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }

    RangoHorario(LocalTime horaInicio, Duration duracion) {
        def horaFin = horaInicio + duracion;
        if(horaFin < horaInicio) {
            throw new Exception("La duracion de fin es menor que la de inicio");
        }
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
    }


    Boolean coincideCon(RangoHorario otroRango) {
        return (otroRango.horaInicio < this.horaInicio && otroRango.horaFin > this.horaInicio) ||
                (otroRango.horaInicio < this.horaFin && otroRango.horaFin > this.horaFin) ||
                (otroRango.horaInicio >= this.horaInicio && otroRango.horaFin <= this.horaFin);
    }

    Boolean contiene(RangoHorario otroRango) {
        return this.horaInicio <= otroRango.horaInicio && this.horaFin >= otroRango.horaFin;
    }

    BigDecimal cantidadDeHoras(){
        return horaInicio.until(horaFin, MINUTES) / 60;
    }

    @Override
    String toString() {
        return "$horaInicio - $horaFin";
    }
}
